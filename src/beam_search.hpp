
#ifndef BEAM_SEARCH
#define BEAM_SEARCH

#include "city.hpp"
#include <algorithm>

using namespace std;

// input:
//   c: the list of cities, used to compute the cost of every state
//   path: the initial state
//   k: the k value
//   iterations: the maximum number of iterations of the main loop
template <typename T>
Path beam_search(const Cities<T>& c, const Path& path, uint k, uint iterations) {
  // state is used to store the current k states
  // newStates is used to store the new states that will be added during each iteration
  // without doing modifications to states
  vector<Path> states(1, path), newStates(1, path);
  // prevCost, cost and timesEqual are used to check if the algorithm has reached a local minimum
  double prevCost = 0., cost = 0.;
  uint timesEqual = 0;

  // stops at: found local minimum and number of iterations
  for (unsigned int cont = 0; timesEqual < 5 && cont < iterations; ++cont) {

    // for each state, generate succesors
    for (size_t s=0; s < states.size(); ++s) {
      // copies the current state
      Path p = states[s];

      // generates all its possible succesors
      for (size_t i=1; i < p.list.size() - 1; ++i)
	for (size_t j=i+1; j<p.list.size(); ++j){

	  // swaps the values
	  swap(p.list[i], p.list[j]);
    // computes the cost of the new states
	  computeCost(c, p);

	  // stores it in the list
	  newStates.push_back(p);

	  // restores the original state to make the next combination
	  swap(p.list[i], p.list[j]);
	}

      // selects the k best states
      // we first sort all new states...
      sort(newStates.begin(), newStates.end(), [](const Path& p1, const Path& p2){ return p1.cost < p2.cost; });
      // ... then we make sure that the duplicate states will not be taken, pushing them to the end of the list...
      unique(newStates.begin(), newStates.end());
      // ... and finally we take the first k states
      newStates.resize( k );
    }
    // we take the new best states as the new current states
    states = newStates;
    // and take the current cost as the cost of the best current states
    cost = states[0].cost;

    // checks if the cost is the same that the cost of the previous iteration
    if (cost == prevCost)
      ++timesEqual;
    else
      timesEqual = 0;

    prevCost = cost;
  }

  // returns the best state
  return states[0];
}

#endif
