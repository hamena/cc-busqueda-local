#ifndef CITY_HPP_
#define CITY_HPP_

#include <cmath>
#include <vector>
#include <iostream>
#include <limits>

using namespace std;

// definition of the parametric class City, which represents a city in a 2D plane
template <typename T>
class City{
public:
  // constructor
  City(const T& x, const T& y) : _x(x), _y(y) {}

  T getX() const { return _x; }
  T getY() const { return _y; }

  // computes the distance from this city to another one
  double distance(const City<T>& c) const{ return sqrt( (_x-c._x)*(_x-c._x) + (_y-c._y)*(_y-c._y) ); }
private:
  T _x, _y;
};

// output operator overload to City<T>
template <typename T>
ostream& operator<<(ostream& os, const City<T>& c) {
  return os << "<" << c.getX() << ", " << c.getY() << ">";
}

// definition of the parametric alias Cities
template <typename T>
using Cities = vector<City<T>>;

// output operator overload to Cities<T>
template <typename T>
ostream& operator<<(ostream& os, const Cities<T>& c) {
  for (const auto& i: c)
    os << i << endl;
	
  return os;
}

// definition of the Path structure, used to store a state
struct Path {
  explicit Path(int size = 0, uint n = 0) : list(size, n), cost(numeric_limits<uint>::max()) {}

  vector<uint> list;
  double cost;
};

// overload of some operators to Path
bool operator==(const Path& p1, const Path& p2) {
  bool equal = true;

  if (p1.list.size() != p2.list.size())
    equal = false;
  else
    for (size_t i = 0; equal && i < p1.list.size(); ++i)
      if (p1.list[i] != p2.list[i])
	equal = false;

  return equal;
}

ostream& operator<<(ostream& os, const Path& p) {
  for (unsigned int i = 0; i < p.list.size(); ++i)
    os << p.list[i] << ", ";

  return os;
}

// definition of the function that will be used to compute
// the cost of a path over a given city
template <typename T>
void computeCost(const Cities<T>& c, Path& p) {

  double cost = 0;

  for (unsigned int i = 0; i < p.list.size() - 1; ++i)
    cost += c[p.list[i]].distance(c[p.list[i+1]]);

  cost += c[p.list.back()].distance(c[p.list.front()]);

  p.cost = cost;

}

#endif // CITY_HPP_
