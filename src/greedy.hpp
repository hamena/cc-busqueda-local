#include <vector>
#include <limits>
#include "city.hpp"

using std::vector;

typedef vector<bool> Visited;

// returns the best available city
template <typename T>
uint select(const Cities<T>& cities, const Visited& visited, uint index){
  T d_min = numeric_limits<T>::max();
  uint i_min = 1;
  size_t maxsize = cities.size();
  for (size_t i=1; i<maxsize; ++i)
    if (!visited[i]){
      T dist = cities[index].distance(cities[i]);
      if (dist < d_min){
	d_min = dist;
	i_min = i;
      }
    }

  return i_min;
}

// common implementation of the greedy algorithm
template <typename T>
Path greedy_TSP(const Cities<T>& cities){
  size_t count = cities.size();
  Visited visited(count,false);
  uint index = 0;
  visited[0] = true;
  Path solution(1,index);
  --count;

  while (count > 0){
    // takes the best available city
    index = select(cities,visited,index);
    // marks it as unavailable
    visited[index] = true;
    // adds it to the solution
    solution.list.push_back(index);
    --count;
  }

  return solution;
}
