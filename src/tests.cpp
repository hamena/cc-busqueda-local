
#include <iostream>
#include <string>
#include <cstdlib>
#include <array>
#include "input.hpp"
#include "city.hpp"
#include "greedy.hpp"
#include "beam_search.hpp"
#include "chronometer.hpp"

using namespace std;

int main() {
  // Definition of the structures and variables we are going to need
  // c is going to be used to maintain the whole list of cities
  Cities<double> c;
  // bestPath: used to store the best path. You are welcome.
  Path bestPath;

  string filename;
  uint k;
  chronometer chr;

  // files contains the name of all the graph-files that will be read
  array<string,15> files = {"a280","berlin52","ch130","ch150","eil101","eil51","eil76","kroA100","kroC100","kroD100","lin105","pcb442","pr76","st70","tsp225"};

  // asking the user for k value...
  cout << "k value: ";
  cin >> k;
  k = 20;
  cout << endl;

  // for each graph-file...
  for (const auto& it : files){
    // print its name
    cout << it << endl;

    // read the corresponding files
    readCities(string("TSPLIB/" + it + ".tsp").c_str(), c);
    readPath(string("TSPLIB/" + it + ".opt.tour").c_str(), bestPath);

    // compute the cost of the best path
    computeCost(c, bestPath);

    // get the initial state using the greedy algorithm...
    Path initial = greedy_TSP(c);
    // ... and compute its cost
    computeCost(c, initial);

    // the call to the beam_search algorithm
    // chr is used to take the measure on the execution time
    chr.start();
    Path p = beam_search(c,initial,k,100);
    chr.stop();

    // this prints the results
    cout << "Greedy: " << initial.cost << ", "
	 << "Local search: " << p.cost << ", "
	 << "Best: " << bestPath.cost << ", "
	 << "Time: " << chr.time() << endl;
  }
}
