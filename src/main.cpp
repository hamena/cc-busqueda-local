
#include <iostream>
#include <string>
#include <cstdlib>
#include "input.hpp"
#include "city.hpp"
#include "greedy.hpp"
#include "beam_search.hpp"
#include "chronometer.hpp"

int main() {
  // definition of the data types we are going to use
  Cities<double> c;

  // c is going to be used to maintain the whole list of cities
  Path bestPath;

  string filename;
  uint k;
  chronometer chr;

  // shows the available graph-files
  cout << "Available data files:" << endl;
  system("ls TSPLIB | cut -d \".\" -f1 | sort | uniq");
  // asks the user to select one
  cout << "File name: ";
  cin >> filename;

  // read the corresponding files
  readCities(string("TSPLIB/" + filename + ".tsp").c_str(), c);
  readPath(string("TSPLIB/" + filename + ".opt.tour").c_str(), bestPath);

  // asking for the k value
  cout << "k value: ";
  cin >> k;
  cout << endl;
	
  // call to computeCost to get the cost of the best path
  computeCost(c, bestPath);

  // get the initial state using the greedy algorithm...
  Path initial = greedy_TSP(c);
  // ... and compute its cost
  computeCost(c, initial);

  // the call to the beam_search algorithm
  // chr is used to take the measure on the execution time
  chr.start();
  Path p = beam_search(c,initial,k,100);
  chr.stop();

  // this prints the results
  cout << "Greedy: " << initial.cost << endl
       << "Local search: " << p.cost << endl
       << "Best: " << bestPath.cost << endl
       << "Time: " << chr.time() << endl;
}
