
#ifndef INPUT_HPP
#define INPUT_HPP

#include <fstream>
#include <ostream>
#include <cstdlib>
#include "city.hpp"

using namespace std;

// reads a graph-file and stores them in a Cities object
template <typename T>
void readCities(const char * fileName, Cities<T>& cities) {

  ifstream ifs(fileName);

  if (ifs.is_open()) {

    cities = Cities<T>();
    char* aux = new char[255];
		
    // we omit the first three lines
    ifs.getline(aux, 255);
    ifs.getline(aux, 255);
    ifs.getline(aux, 255);

    // we omit the string of the fourth line and take the number of cities
    ifs.getline(aux, 255);
    unsigned int numVertices = atoi(aux+11);

    // omitting two more lines...
    ifs.getline(aux, 255);
    ifs.getline(aux, 255);

    // in this loop we read the coordinates of all the cities
    for (unsigned int i = 0; i < numVertices; ++i) {
      T x, y;
      unsigned int v;

      ifs >> v >> x >> y;
      cities.push_back(City<T>(x, y));
    }

    ifs.close();

  } else {
    cerr << "Error: file \"" << fileName << "\" can not be opened." << endl;
    exit(0);
  }

}

// reads a path from a solution file
void readPath(const char * fileName, Path& path) {

  ifstream ifs(fileName);

  if (ifs.is_open()) {

    path = Path();

    char* aux = new char[255];
		
    // we omit the first two lines
    ifs.getline(aux, 255);
    ifs.getline(aux, 255);

    // we omit the string of the fourth line and take the number of cities
    ifs.getline(aux, 255);

    // some files contains an additional line right here, so we are checking that
    if (aux[0] != 'D')
      ifs.getline(aux, 255);

    unsigned int numVertices = atoi(aux+11);

    // omitting one more line...
    ifs.getline(aux, 255);

    // reading all the cities...
    for (unsigned int i = 0; i < numVertices; ++i) {
      unsigned int v;

      ifs >> v;
      path.list.push_back(v-1);
    }

    ifs.close();

  } else {
    cerr << "Error: file \"" << fileName << "\" can not be opened." << endl;
    exit(0);
  }

}

#endif 
